GitHub Pages: https://hcuffy.github.io/Marks/


## Installation

First, clone the repo via git:

```bash
git clone https://github.com/hcuffy/Marks
```

Then install the dependencies with yarn.

```bash
$ cd marks
$ yarn
```

Then to launch the app

```bash
$ yarn dev
```

OR for production version

```bash
$ yarn start
```

## A Quick Look

<div align="center">

  <img src="./internals/img/app.png" />

</div>

Developed using [**electron-react-boilerplate**](https://github.com/electron-react-boilerplate/electron-react-boilerplate)
