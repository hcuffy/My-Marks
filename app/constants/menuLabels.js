export const ENGLISH_LABELS = {
    hide:   'Hide Marks',
    about:  'About Marks',
    others: 'Hide Others',
    quit:   'Quit',
    view:   'View',
    toggle: 'Toggle Full Screen',
    window: 'Window',
    mini:   'Minimize',
    close:  'Close',
    front:  'Reopen'
};

export const GERMAN_LABELS = {
    hide:   'Ausblenden: Marks',
    about:  'Über Marks',
    others: 'Andere ausblenden',
    quit:   'Marks beenden',
    view:   'Ansicht',
    toggle: 'Vollbildmodus',
    window: 'Fenster',
    mini:   'Minimieren',
    close:  'Schließen',
    front:  'Wieder Öffnen'
};
