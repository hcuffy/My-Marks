import React from 'react';
import _ from 'lodash';
import {Dropdown, DropdownItem, DropdownMenu, DropdownToggle} from 'reactstrap';
import {firstMakeSelection} from '../../notifications/warnings';
import css from './styles/helpers.css';

export const getClassList = classlist => {
    return classlist.map((data, idx) => (
        <DropdownItem
            key={idx}
            name={data.name}
            data-id={data._id}
            data-check='classDropdown'
        >
            {data.name}
        </DropdownItem>
    ));
};

export const getSubjectList = (classroom, subjectData) => {
    const selectedSubjects = _.filter(subjectData.data, [
        'classroomId',
        classroom.selectedRoom
    ]);

    return selectedSubjects.map((data, idx) => (
        <DropdownItem
            key={idx}
            name={data.name}
            data-id={data._id}
            data-check='subjectDropdown'
        >
            {data.name}
        </DropdownItem>
    ));
};

export const getAllSubjects = subjects => {
    const checkSubject = _.isUndefined(subjects) ? [] : subjects;

    return checkSubject.map((data, idx) => (
        <DropdownItem
            key={idx}
            name={data.name}
            data-id={data._id}
            data-check='subjectDropdown'
        >
            {data.name}
        </DropdownItem>
    ));
};

export const getExamList = (exams, subjectId) => {
    const selectedExams = _.filter(exams, ['subjectId', subjectId]);

    return selectedExams.map((data, idx) => (
        <DropdownItem
            key={idx}
            name={data.subjectId}
            data-id={data._id}
            data-check='examDropdown'
        >
            {data.title}
        </DropdownItem>
    ));
};

export const getStudentList = allStudents => {
    const students = _.sortBy(
        _.isUndefined(allStudents) ? [] : allStudents,
        ['firstname'],
        ['asc']
    );

    return students.map((data, idx) => (
        <DropdownItem key={idx} data-id={data._id} data-check='studentDropdown'>
            {`${data.firstname} ${data.lastname}`}
        </DropdownItem>
    ));
};

export const getNotesList = (allNotes, studentId) => {
    if (_.isNull(studentId) || allNotes.length === 0) {
        return [];
    }

    const notes = _.sortBy(_.filter(allNotes, {studentId}), ['title'], ['asc']);

    return notes.map((data, idx) => {
        const created = _.toString(data.createdAt).substring(4, 15);

        return (
            <DropdownItem key={idx} data-id={data._id} data-check='notesDropdown'>
                {`${data.title} (${created})`}
            </DropdownItem>
        );
    });
};

export const subjectOptions = (subjects, actions) => {
    return subjects.map((data, idx) => (
        <DropdownItem
            key={idx}
            name={data.name}
            onClick={actions.showSubject}
            data-check='classDropdown'
        >
            {data.name}
        </DropdownItem>
    ));
};

const heightModifier = {
    setMaxHeight: {
        enabled: true,
        fn:      data => ({
            ...data,
            styles: {
                ...data.styles,
                overflow:  'auto',
                maxHeight: 300
            }
        })
    }
};

export const createDropdown = (
    styling,
    openIt,
    action,
    label,
    options,
    dataId
) => (
    <div className={styling}>
        <Dropdown isOpen={openIt} toggle={action}>
            <DropdownToggle data-check={dataId} className={css.dropdown_color} caret>
                {label}
            </DropdownToggle>
            <DropdownMenu modifiers={heightModifier}>{options}</DropdownMenu>
        </Dropdown>
    </div>
);

export const notifyIfEmpty = (t, options, selected, section) => {
    if (_.isEmpty(options) && selected) {
        firstMakeSelection(t, section);
    }
};

export const getClassroomName = (id, classdata) => {
    const classObject = _.find(classdata, {_id: id});
    if (_.isUndefined(classObject)) {
        return '';
    }

    return classObject.name;
};

export const getClassroomId = (name, classdata) => {
    const classObject = _.find(classdata, {name});
    if (_.isUndefined(classObject)) {
        return '';
    }

    return classObject._id;
};

export const getQuestionList = (
    t,
    classroomId,
    capabilityQuestions,
    {updateQuestionSet}
) => {
    return capabilityQuestions.map((data, idx) => (
        <DropdownItem
            key={idx}
            name={data.name}
            data-id={classroomId}
            onClick={updateQuestionSet}
            data-check='questionDropdown'
        >
            {t(`capability.${data.name}.name`)}
        </DropdownItem>
    ));
};
