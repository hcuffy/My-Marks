export const chartOptions = () => ({
    maintainAspectRatio: false,
    scales:              {
        yAxes: [
            {
                ticks: {
                    beginAtZero: true
                }
            }
        ]
    }
});
